# Create a full local test environment
These are full instructions to create a local testing environment.  Production servers may have bandwidth limitations, especially for large files.
Tested with multipass on edge via snap.

##Set up sp-test-RemoteSSH server
Just run ./multipass-RemoteSSH.sh

This will do something like:
1. multipass launch 18.04 -n RemoteSSH
2. mkdir 1010101
3. sudo sosreport --batch
4. sudo chown ubuntu /tmp/sosreport-RemoteSSH*
5. sudo mv /tmp/sosreport-RemoteSSH* 1010101/

##Set up sp-test
First run ./multipass-sp-test.sh which will do something similar to the below

Creates a new VM with clones repo
2. give it access to other VMs via keys..
sudo multipass copy-files /var/snap/multipass/common/data/multipassd/ssh-keys/id_rsa sp-test:/home/ubuntu/.ssh/id_rsa
3. multipass shell sp-test
(Now In sp-test Shell)
4. Make key permissions correct.. chmod og-rw .ssh/id_rsa
5. cp .ssh/authorized_keys .ssh/id_rsa.pub
6. clone this repo.. git clone https://gitlab.com/BryanQuigley/ScoutPlane.git
7. install dependencies.. sudo apt install python3-paramiko python3-pylxd

These you have to do yourself (almost works.. hostname issues)
8. sudo lxd init (choose all defaults except choose dir as storage backend instead of btrfs)
9. run ./createlxdimage.sh
10. cp scoutplane.ini-example scoutplane.ini
#Modify it if your name, keys, etc are different. Or if name resolution doesn't work.
11. Actually run ./sp /home/ubuntu/1010101/sosreport-RemoteSSH-20180425145608.tar.xz (replacing this filename with one that actually exists)
