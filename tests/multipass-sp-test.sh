#!/usr/bin/env bash
multipass launch 18.04 -n sp-test
multipass exec sp-test -- git clone https://gitlab.com/BryanQuigley/ScoutPlane.git
multipass exec sp-test -- sudo apt install python3-paramiko python3-pylxd --assume-yes
sudo multipass copy-files /var/snap/multipass/common/data/multipassd/ssh-keys/id_rsa sp-test:/home/multipass/.ssh/id_rsa
multipass exec sp-test -- sudo chmod og-rw .ssh/id_rsa
multipass exec sp-test -- cp .ssh/authorized_keys .ssh/id_rsa.pub
