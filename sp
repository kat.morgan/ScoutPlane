#!/usr/bin/env python3
import paramiko
import os
import argparse
import sys
import configparser
from stat import S_ISDIR
from pathlib import Path
import lxdhelper
import pylxd
client = pylxd.Client()

home = str(Path.home())
homeconfig = str(Path.home()) + '/.config/scoutplane.ini'

config = configparser.ConfigParser()

# Get file from remote server and put in a container for that case# (derived from directory name)


def GetHumanReadable(size, precision=2):
    # Credit to Pavan Gupta https://stackoverflow.com/questions/5194057/
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB']
    suffixIndex = 0
    while size > 1024 and suffixIndex < 4:
        suffixIndex += 1
        size = size/1024.0
    return "%.*f%s" % (precision, size, suffixes[suffixIndex])


def VerifyMD5Sum(container, cont_filename_tar_dir, filename, md5sum):

    print("Verify: \tStarting")
    verify = container.execute(['md5sum', cont_filename_tar_dir + filename]).stdout[0:32]
    if str(verify) == str(md5sum):
        print("Verify: \tComplete")
    else:
        print("Verify: \tFAILED")


def DownloadFiletoContainer(sftp, container, cont_filename_tar_dir, filename, remotepath):
    filestat = sftp.stat(remotepath)
    print("The file being downloaded is " + GetHumanReadable(filestat.st_size))

    print("Opening " + remotepath)
    fileptr = sftp.open(remotepath, mode='r')
    fileptr.prefetch(filestat.st_size)

    print("Writing to container")
    container.files.put(cont_filename_tar_dir + filename, fileptr)

    try:
        md5sum = sftp.open(remotepath + ".md5").readline()[0:32]
        VerifyMD5Sum(container, cont_filename_tar_dir, filename, md5sum)
    except OSError as e:
        print("Verify: \tCan't verify download, no md5sum exist at source")
        return

    VerifyMD5Sum(container, cont_filename_tar_dir, filename)


def MoveLocalFiletoContainer(container, localpath, filename, cont_filename_tar_dir):
    filestat = os.stat(localpath)
    print("The file being moved is " + GetHumanReadable(filestat.st_size))

    print("Opening " + localpath)
    fileptr = open(localpath, 'rb').read()

    print("Writing to container")
    container.files.put(cont_filename_tar_dir + filename, fileptr)

    try:
        md5sum = open(localpath + ".md5").readline()[0:32]
        VerifyMD5Sum(container, cont_filename_tar_dir, filename, md5sum)
    except OSError as e:
        print("Verify: \tCan't verify download, no md5sum exist at source")
        return


def SOSAnalysis(container, cont_filename_tar_dir, filename):
    print("Extract: \tStarting")
    extraction = container.execute(['tar', 'xfJ', cont_filename_tar_dir + filename, '-C', cont_filename_tar_dir, '--exclude=dev/null'])
    if(extraction[0]) == 0:
        print("Extract: \tComplete")
    else:
        print("Extract: \tPossible error, analysis stopping")
        print(extraction)
        sys.exit(2)  # Give up..
    xsos = container.execute(['xsos', '-cmbodlesngisp', cont_filename_tar_dir + '*/'])
    if(xsos[0]) == 0:
        print('xsos: \t\tSuccess')
        container.files.put(cont_filename_tar_dir + 'xsos', xsos[1].encode(encoding="utf-8", errors="ignore"))
        container.files.put(cont_filename_tar_dir + 'xsos.stderr', xsos[2].encode(encoding="utf-8", errors="ignore"))
    else:
        print('xsos: \tPossible error')
        print(xsos)


def InitContainer(container_name):
    if client.containers.exists(container_name):
        print("Existing container found, using: " + container_name)
        container = client.containers.get(container_name)
    else:
        print("No container found, creating a new one: " + container_name)
        containerconfig = {'name': container_name, 'source':
                           {'type': 'image', 'alias': 'sp-base-image'}}
        container = client.containers.create(containerconfig, wait=True)

    lxdhelper.StartContainer(container)
    return container


# Break if no config file exists
if not os.path.exists(homeconfig) and not os.path.exists('scoutplane.ini'):
    print(homeconfig)
    print("No configuration file detected! Let's make you one.")
    print("Enter server name:")
    server_name = input()
    print("User name:")
    user_name = input()
    config['REMOTE'] = {'Server': server_name, 'Username': user_name}
    config.write(open(homeconfig, 'w'))
    print("If everything went well, please rerun this command now with config")
    sys.exit()

# Create images if none exist
try:
    lxd_image = client.images.get_by_alias("sp-base-image")
except pylxd.exceptions.NotFound as e:
    print("No image exists, let's make one!")
    lxdhelper.createlxdimage()
    print("If everything went well, please rerun this command now that images are defined")
    sys.exit(1)

config.sections()
config.read(homeconfig)
config.read('scoutplane.ini')

parser = argparse.ArgumentParser()
parser.add_argument("path", help="full remote file path aka /customers/ABCinc/10101/sosreport-xyz.tar.xz")
parser.add_argument("-c", "--containername", help="specify container name to override autodetected case number from directory name, recommended if using local")
location = parser.add_mutually_exclusive_group()
location.add_argument("-l", "--local", help="use local filesystem", action="store_true")
location.add_argument("-r", "--remote", help="[default] use remote filesystem", action="store_true")
args = parser.parse_args()

filename = os.path.basename(args.path)

if args.containername:
    container_name = "sp-" + args.containername
else:
    dirname = os.path.dirname(args.path).rsplit("/", 1)
    casenumber = dirname[1]
    container_name = "sp-" + casenumber

if args.local:
    try:
        filestat = os.stat(args.path)  # Partially redundant, should redo to check the whole directory
    except FileNotFoundError as e:
        print("Error: File doesn't exist on remote machine")
        sys.exit(2)
else:
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    try:
        ssh.connect(hostname=config['REMOTE']['Server'], username=config['REMOTE']['Username'])
    except paramiko.SSHException as e:
        print("Connection Error")
        print(e)
        sys.exit(2)
    except KeyError as e:
        print("Error: Maybe scoutplane.ini isn't configured correctly?")
        sys.exit(2)

    sftp = ssh.open_sftp()

    # Remote: Check if file exists on RemoteSSH
    try:
        filestat = sftp.stat(args.path)  # Partially redundant, should redo to check the whole directory
    except FileNotFoundError as e:
        print("Error: File doesn't exist on remote machine")
        sys.exit(2)

# Remote: Check if directory (and quit but eventually do more)
if S_ISDIR(filestat.st_mode):
    print("Error: You've given us a directory and we don't have magic for that yet, sorry")
    sys.exit(2)

container = InitContainer(container_name)

# Create work working directory just for this file
cont_filename_tar_dir = '/home/ubuntu/D' + filename + '/'
print(container.execute(['mkdir', cont_filename_tar_dir]))

if args.local:
    MoveLocalFiletoContainer(container, args.path, filename, cont_filename_tar_dir)
else:
    DownloadFiletoContainer(sftp, container, cont_filename_tar_dir, filename, args.path)
    sftp.close()
    # evaluate if we still need this - ssh.close()

# If it's likely a sosreport, let's do some analysis.
if filename.startswith('sosreport') and filename.endswith("tar.xz"):
    SOSAnalysis(container, cont_filename_tar_dir, filename)

print("\nPotentially helpful things to do next")
print("lxc exec " + container_name + " bash")
print("cd " + cont_filename_tar_dir)

# vim: set et ts=4 sw=4 :
